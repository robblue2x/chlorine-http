/* globals browser */

const t = browser.i18n.getMessage;

const removeWarnings = () => {
  const warnings = document.getElementsByClassName('chlorine-http-warning');
  for (const w of warnings) {
    document.body.removeChild(w);
  }
};

const addWarning = () => {
  if (window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1') {
    return;
  }

  const warning = document.createElement('div');
  const info = document.createElement('a');
  info.href = t('link');
  info.innerText = t('insecure');
  warning.append(info);
  warning.append('  ');

  const newLink = document.createElement('a');
  newLink.href = window.location.href.replace('http://', 'https://');
  newLink.innerText = t('trySecure');
  warning.append(newLink);
  warning.append('  ');

  const closeLink = document.createElement('a');
  closeLink.innerText = t('close');
  closeLink.style = { cursor: 'pointer' };
  closeLink.onclick = () => {
    document.body.removeChild(document.getElementsByClassName('chlorine-http-warning')[0]);
  };
  warning.append(closeLink);

  warning.className = 'chlorine-http-warning';

  document.body.append(warning);
};

removeWarnings();
addWarning();
